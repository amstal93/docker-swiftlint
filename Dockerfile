ARG SWIFT_VERSION_TAG="5.1.4"
FROM swift:${SWIFT_VERSION_TAG}-bionic
ARG SWIFTLINT_TAG="0.39.1"

LABEL maintainer="Twilio Flutter"

# Disable ruby deprecation warnings
ENV RUBYOPT='-W0'

ENV SHELL /bin/bash

# Install dependencies
RUN apt-get update -y \
    && apt-get -y --no-install-recommends install \
        git procps wget libsqlite3-dev libncurses-dev

WORKDIR /opt

# Building SwiftLint
RUN git clone --branch ${SWIFTLINT_TAG} https://github.com/realm/SwiftLint.git && \
    cd SwiftLint && \
    swift build --configuration release --static-swift-stdlib && \
    mv `swift build --configuration release --static-swift-stdlib --show-bin-path`/swiftlint /usr/bin && \
    cd .. && \
    rm -rf SwiftLint

# Clean up build dependencies
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app